import React from 'react'; // eslint-disable-line
import { Navigation } from 'react-native-navigation';
import { Provider } from 'react-redux';
import { registerScreens } from './screens';
import configureStore from './store/configureStore';

const store = configureStore();


registerScreens(store, Provider);

const navigatorStyle = {
	statusBarColor: 'black',
	statusBarTextColorScheme: 'light',
	navigationBarColor: 'black',
	navBarBackgroundColor: '#0a0a0a',
	navBarTextColor: 'white',
	navBarButtonColor: 'white',
	tabBarButtonColor: 'red',
	tabBarSelectedButtonColor: 'red',
	tabBarBackgroundColor: 'white',
	topBarElevationShadowEnabled: false,
	navBarHideOnScroll: true,
	tabBarHidden: false,
};

Navigation.startTabBasedApp({
  tabs: [
		{
					label: 'Список',
					screen: 'testapp.List',
					icon: require('./one.png'),
					title: 'Список',
					navigatorStyle,
				},
				{
					label: 'Элемент',
					screen: 'testapp.Item',
					icon: require('./two.png'),
					title: 'Элемент',
					navigatorStyle
				}
	],

});
