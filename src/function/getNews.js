export const getNews = async () => {
    let news;
    try {
      await fetch(`https://newsapi.org/v2/top-headlines?country=us&apiKey=2afacfeda5bf413a9dfc1e2a75b189e5`)
        .then(res => res.json())
        .then(res => {
            news = res;
        })
    } catch(e){
      throw e;
    }
    return news;
};
