import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import {
	Text,
	View,
	TouchableOpacity,
	ToastAndroid,
	FlatList,
	Linking,
	Button,
	Image,
	ActivityIndicator
} from 'react-native';
import { addNews } from "./store/actions/index";
import { clickNews } from "./store/actions/index";
import { addItem } from "./store/actions/index";
import { getNews } from "./function/getNews";

class List extends Component {
	constructor(props) {
		super(props);
		this.state = {
			index: null,
			trigger: false,
		}
	}

	componentDidMount(){
		/*fetch(`https://newsapi.org/v2/top-headlines?country=us&apiKey=2afacfeda5bf413a9dfc1e2a75b189e5`)
      .then(res => res.json())
      .then(res => {
        console.log(res);
      })
      .catch(err => {
        console.error(err);
      });*/

			getNews()
						.then(result => {
							this.props.onAddNews(result);
							this.setState({ trigger: true })
						})
						.catch(error => {
							console.log(error);
						});
	}

	Picture(url){
		return (
		<View style={{ justifyContent: 'center', flexDirection: 'row'}}>
			<Image
				 source={{ uri: `${url}` }}
				 style={{ width: 80, height: 80 }}
				 resizeMethod= 'resize'
			/>
	  </View>
		);
	}

	noPicture(){
		return (
		<View style={{ justifyContent: 'center', flexDirection: 'row'}}>
			<Text>Картинка отсутствует</Text>
		</View>
		);
	}

	onClick(index){
		let item = this.props.list.news.articles[index];
		this.props.onAddItem(item);
		console.log(this.props.item);
	}

	_renderContent(){
		return (
			<FlatList
			data={this.props.list.news.articles}
			renderItem={({ item, index }) => (
					<View style={{ padding: 10, marginTop: 30, borderWidth: 1, borderColor: 'green'}}>
						<Text>{item.title}</Text>
						{item.urlToImage ? this.Picture(item.urlToImage) : this.noPicture()}
						<View style={{padding: 40}}>
							<Button onPress={() => Linking.openURL(`${item.url}`)} title='Перейти по ссылке'></Button>
						</View>
						<View style={{padding: 40}}>
							<Button onPress={() => this.onClick(index)} title='Выбор элемента списка'></Button>
						</View>
					</View>
			)}
			keyExtractor={(item, index) => index}
			/>
		);
	}

	_renderSpinner(){
		return (
			<View style ={{ justifyContent: 'center', flexDirection: 'row', alignItems: 'center', height: '100%'}}>
				<ActivityIndicator size="large" color="#0000ff" />
			</View>
		);
	}



	render() {
		let content = this.state.trigger ? this._renderContent() : this._renderSpinner();
		return (
			<View>
				{content}
			</View>
		);
	}
}

const mapStateToProps = state => {
	return {
		list: state.list,
		item: state.item
	};
};

const mapDispatchToProps = dispatch => {
	return {
		onClickNews: index => dispatch(clickNews(index)),
		onAddNews: news => dispatch(addNews(news)),
		onAddItem: item => dispatch(addItem(item)),
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(List);
