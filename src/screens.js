/* eslint-disable import/prefer-default-export */
import { Navigation } from 'react-native-navigation';

import List from './List';
import Item from './Item';

export function registerScreens(store, Provider) {
	Navigation.registerComponent('testapp.List', () => List, store, Provider);
	Navigation.registerComponent('testapp.Item', () => Item, store, Provider);
}
