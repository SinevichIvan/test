import {
  ADD_ITEM,
} from "../actions/actionTypes";

const initialState = {
  data: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_ITEM:
      return {
        data: action.payload
      };
    default:
      return state;
  }
};

export default reducer;
