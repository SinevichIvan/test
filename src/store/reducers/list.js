import {
  ADD_NEWS,
  CLICK_NEWS
} from "../actions/actionTypes";

const initialState = {
  news: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_NEWS:
      return {
        news: action.payload
      };
    case CLICK_NEWS:
      return {
        news: action.payload
      };
    default:
      return state;
  }
};

export default reducer;
