import { ADD_NEWS, CLICK_NEWS, ADD_ITEM } from './actionTypes';

export const addNews = (news) => {
    return {
        type: ADD_NEWS,
        payload: news
    };
};

export const clickNews = (index) => {
    return {
        type: CLICK_NEWS,
        payload: index
     };
};

export const addItem = (item) => {
    return {
        type: ADD_ITEM,
        payload: item
     };
};
