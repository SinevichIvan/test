import React, { Component, PropTypes } from 'react';
import {
	Text,
	View,
	TouchableOpacity,
	ToastAndroid,
	Button,
	Image
} from 'react-native';
import { connect } from 'react-redux';


class Item extends Component {
	constructor(props) {
		super(props);
	}

	Picture(url){
		return (
		<View style={{ justifyContent: 'center', flexDirection: 'row'}}>
			<Image
				 source={{ uri: `${url}` }}
				 style={{ width: 80, height: 80 }}
				 resizeMethod= 'resize'
			/>
	  </View>
		);
	}

	noPicture(){
		return (
		<View style={{ justifyContent: 'center', flexDirection: 'row'}}>
			<Text>Картинка отсутствует</Text>
		</View>
		);
	}

	_renderContent(){
		return (
			<View style={{ padding: 10, marginTop: 30, borderWidth: 1, borderColor: 'green'}}>
				<Text>{this.props.item.data.title}</Text>
				{this.props.item.data.urlToImage ? this.Picture(this.props.item.data.urlToImage) : this.noPicture()}
				<View style={{padding: 40}}>
					<Button onPress={() => Linking.openURL(`${this.props.item.data.url}`)} title='Перейти по ссылке'></Button>
				</View>
			</View>
		);
	}

	_renderEmpty(){
		return (
			<Text>Ничего нет</Text>
		);
	}

	render() {
		console.log(this.props.item);
		let content = this.props.item.data != null ? this._renderContent() : this._renderEmpty();
		return (
			<View>
				{content}
			</View>
		);
	}
}

const mapStateToProps = state => {
	return {
		item: state.item
	};
};

export default connect(mapStateToProps, null)(Item);
